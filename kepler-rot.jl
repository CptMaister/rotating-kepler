
# this code has been written with a lot of inspiration from https://docs.sciml.ai/SciMLTutorialsOutput/html/models/05-kepler_problem.html and a lot of jank workarounds instead of properly learning how to use julia

# run by starting the Julia REPL in a terminal (type 'julia' there) and then entering: include("path/to/script-name.jl")



#####***** user input for different rotating ellipses *****#####
# set l and k for period T = 2 * pi * (l/k)
l_period = 3
k_period = 2

# set angular momentum L
L_initial = 1

# set inverse of tdelta, i.e. time step will be (1 / 16) if input is tdeltainv = 16
tdeltainv = 16
#####***** end user input *****#####



using OrdinaryDiffEq, LinearAlgebra, ForwardDiff, Plots; gr()

### calculate E from l and k
E_initial = cbrt((k_period / l_period)^2) * (-1 / 2)

### calculate q and p from E_initial and L, starting on the x-axis
q2_init = 0
p1_init = 0
q1_init = (-1 + sqrt(1 + (4 * E_initial * (L_initial^2 / 2)))) / (2*E_initial)
p2_init = L_initial / q1_init

initial_position = [q1_init, q2_init]
initial_velocity = [p1_init, p2_init]
initial_cond = (initial_position, initial_velocity)




### calculate time stuff
tdelta = 1/tdeltainv
tstart = 0
tendmax = floor(Integer, 500000 / tdeltainv) # close to 500 000 time points is easily manageable for medium laptops in a few seconds
T_single_ellipse_period = 2 * pi * (l_period / k_period) # calculated from l and k so that the orbit passes a single period
tendperiod = ceil(Integer, (k_period / gcd(k_period, l_period)) * T_single_ellipse_period)
tend = min(tendperiod, tendmax)
tsamplesnumber = ((tend - tstart) * tdeltainv) + 1 # use tdeltainv to avoid floating point rounding errors a little
time_data = range(0, tend, length=tsamplesnumber)
tspan = (tstart,tend)




### ODE stuff
E(q,p) = norm(p)^2/2 - inv(norm(q)) # this was originally called H in the copied code
# L(q,p) = q[1]*p[2] - p[1]*q[2]

pdot(dp,p,q,params,t) = ForwardDiff.gradient!(dp, q->-E(q, p), q)
qdot(dq,p,q,params,t) = ForwardDiff.gradient!(dq, p-> E(q, p), p)

prob = DynamicalODEProblem(pdot, qdot, initial_velocity, initial_position, tspan)

print("calculating... ")
sol = solve(prob, KahanLi6(), dt=tdelta);




### now rotate the orbits that were calculated by solve

# rot(alpha) = [cos(alpha) -sin(alpha); sin(alpha) cos(alpha)]
rot_left(alpha) = [cos(alpha); sin(alpha)]
rot_right(alpha) = [-sin(alpha); cos(alpha)]

splat(f) = x -> f(x...) # see https://stackoverflow.com/questions/66322267/evaluate-vectors-or-tuples-in-a-function-julia

q1_data = sol[3,:] # ohne Rotation
q2_data = sol[4,:] # ohne Rotation

rotations_from_times_left = splat(rot_left).(time_data)
rotations_from_times_right = splat(rot_right).(time_data)

rotated_q_left = rotations_from_times_left.*q1_data # so this is just the left column of the rotation matrix multiplied with the x values of the not-rotated q, so of q1
rotated_q_right = rotations_from_times_right.*q2_data # now with the right column and q2
rotated_q = rotated_q_left + rotated_q_right

rotated_q_matrix = permutedims(hcat(rotated_q...))
x_data = rotated_q_matrix[:, 1]
y_data = rotated_q_matrix[:, 2]




### plot orbit

# plot_orbit(sol) = plot(time_data, q1, q2, lab="Orbit", title="Kepler Problem Solution")
plot_orbit(sol) = plot(
  x_data,
  y_data,
  lab="Orbit",
  title="Rotating Kepler Problem Solution",
  size=(600,600)
)

print("plotting... ")
plot_orbit(sol)


