
# this code has been written with a lot of inspiration from https://docs.sciml.ai/SciMLTutorialsOutput/html/models/05-kepler_problem.html and a lot of jank workarounds instead of properly learning how to use julia

using Plots, OrdinaryDiffEq, LinearAlgebra, DiffEqPhysics; gr()


# run by starting the Julia REPL in a terminal (type 'julia' there) and then entering: include("path/to/script-name.jl")


#####***** user input for different rotating ellipses *****#####
# set l and k for period T = 2 * pi * (l/k)
l_period = 3
k_period = 2

time_3d_plot = false

# set angular momentum L
L_initial = 1.1

# set inverse of tdelta, i.e. time step will be (1 / 16) if input is tdeltainv = 16
tdeltainv = 2^12
# tdeltainv = 2^16
#####***** end user input *****#####


# inputs k und l kürzen:
greatest_common_divisor = gcd(k_period, l_period)
l_period = l_period / greatest_common_divisor
k_period = k_period / greatest_common_divisor

### calculate E from l and k
E_initial = cbrt((k_period / l_period)^2) * (-1 / 2)

println(string("l, k, L and E:\t", join([l_period, k_period, L_initial, E_initial], ",\t")))


### calculate some special values
L_abs_upper_bound = ((l_period / k_period)^2)^(1/6) # = sqrt(-1 / (2 * E_initial))
println(string("Upper bound of |L|:\t\t ", L_abs_upper_bound))

runge_norm_squared = 1 + (2 * E_initial * L_initial^2)
excentricity = sqrt(runge_norm_squared)
println(string("Excentricity e = ||A||:\t\t ", excentricity))


### calculate q and p from E_initial and L, starting on the x-axis
q2_init = 0
p1_init = 0
q1_init = (-1 + excentricity) / (2*E_initial)
p2_init = L_initial / q1_init

initial_position = [q1_init, q2_init]
initial_velocity = [p1_init, p2_init]
initial_cond = (initial_position, initial_velocity)



### calculate time stuff
tdelta = 1/tdeltainv
tstart = 0
tendmax = floor(Integer, 1000000000 / tdeltainv) # close to 500 000 time points is easily manageable for medium laptops in a few seconds
T_single_ellipse_period = 2 * pi * (l_period / k_period) # calculated from l and k so that the orbit passes a single period
tendperiod = ceil(Integer, k_period * T_single_ellipse_period)
tend = min(tendperiod, tendmax)
tsamplesnumber = ((tend - tstart) * tdeltainv) + 1 # use tdeltainv to avoid floating point rounding errors a little
time_data = range(0, tend, length=tsamplesnumber)
tspan = (tstart,tend)

println(string("\ntime end:\t\t ", tend))
println(string("time delta⁻¹:\t\t ", tdeltainv))
println(string("time sample points:\t ", tsamplesnumber))



### Hamiltonians
H(p,q) = E(p,q) + L(p,q)
E(p,q) = norm(p)^2/2 - inv(norm(q))
L(p,q) = q[1]*p[2] - p[1]*q[2]


### check values
rounded_E_initial = round(E_initial, digits=17)
rounded_E_recalculated = round(E(initial_velocity, initial_position), digits=17)
rounded_L_initial = round(L_initial, digits=17)
rounded_L_recalculated = round(L(initial_velocity, initial_position), digits=17)

if rounded_E_initial != rounded_E_recalculated || rounded_L_initial != rounded_L_recalculated
  println("WARNING! Initial values for E and L are different from recalculated values.")
  println(E_initial)
  println(E(initial_velocity, initial_position))
  println(L_initial)
  println(L(initial_velocity, initial_position))
end


### construct Hamiltonian problem, see https://docs.sciml.ai/DiffEqDocs/latest/types/dynamical_types/
prob = HamiltonianProblem((p,q,param) -> H(p,q), initial_velocity, initial_position, tspan)


print("\n calculating...")
sol = solve(prob, SymplecticEuler(), dt=tdelta);
# sol = solve(prob, KahanLi6(), dt=tdelta);
# sol = solve(prob, SofSpa10(), dt=tdelta);

q1_data = sol[3,:]
q2_data = sol[4,:]
# u = sol.u
# print(string("size sol: ", size(sol)))
# print(string("size sol.u: ", size(sol.u)))

if time_3d_plot
  plot_data = [time_data, q1_data, q2_data]
else
  plot_data = [q1_data, q2_data]
end

plot_orbit() = plot(
  plot_data...,
  lab="Orbit",
  title="Rotating Kepler Problem Solution",
  size=(600,600)
)

print(" plotting...\n")
plot_orbit()
